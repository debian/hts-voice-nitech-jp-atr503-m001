hts-voice-nitech-jp-atr503-m001 (1.05-8) unstable; urgency=medium

  * d/control: move to Debian TTS Maintainers from individuals.
    Thanks to Koichi Akabe <vbkaisetsu@gmail.com> for previous works.
  * Use secure URI in Homepage and Source fields.
  * d/salsa-ci.yml: disable salsa-ci blhc and build package any tests.
  * Update standards version to 4.6.2.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Mon, 16 Oct 2023 13:02:42 +0900

hts-voice-nitech-jp-atr503-m001 (1.05-7) unstable; urgency=medium

  * add d/salsa-ci.yml

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sun, 10 Jul 2022 18:35:16 +0900

hts-voice-nitech-jp-atr503-m001 (1.05-6) unstable; urgency=medium

  [ Debian Janitor ]
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Archive.

  [ HIGUCHI Daisuke (VDR dai) ]
  * Bump debhelper compatibility level to 13
  * set Rules-Requires-Root as no.
  * eliminate lintian warning:
    - older-debian-watch-file-standard
    - update-debian-copyright
    - missing-explanation-for-contrib-or-non-free-package
    - upstream-metadata-missing-{bug-tracking,repository}
  * Update Standards-Version to 4.6.1

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sun, 03 Jul 2022 17:23:33 +0900

hts-voice-nitech-jp-atr503-m001 (1.05-5) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/watch: Use https protocol

  [ HIGUCHI Daisuke (VDR dai) ]
  * eliminate lintian warning: file-contains-trailing-whitespace
  * Bump debhelper compatibility level to 12
  * Update Standards-Version to 4.4.0

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sat, 03 Aug 2019 13:09:52 +0900

hts-voice-nitech-jp-atr503-m001 (1.05-4) unstable; urgency=medium

  [ HIGUCHI Daisuke (VDR dai) ]
  * welcome back Koichi Akabe <vbkaisetsu@gmail.com> for co-maintainer.
  * run wrap-and-sort
  * Move Vcs-* to salsa.debian.org
  * eliminate lintian warning: priority-extra-is-replaced-by-priority-optional
  * Bump debhelper compatibility level to 11
  * Update Standards-Version to 4.1.3

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Mon, 15 Jan 2018 18:35:27 +0900

hts-voice-nitech-jp-atr503-m001 (1.05-3) unstable; urgency=medium

  * debian/control: add Vcs-* headers.

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Mon, 10 Jul 2017 19:31:28 +0900

hts-voice-nitech-jp-atr503-m001 (1.05-2) unstable; urgency=medium

  * New maintainer.
    thanks to Koichi Akabe <vbkaisetsu@gmail.com> for previous works.
  * debian/copyright:
    eliminate lintian warning: space-in-std-shortname-in-dep5-copyright
  * bump up Standards-Version 4.0.0

 -- HIGUCHI Daisuke (VDR dai) <dai@debian.org>  Sun, 09 Jul 2017 23:42:56 +0900

hts-voice-nitech-jp-atr503-m001 (1.05-1) unstable; urgency=medium

  * Migrate to unstable
  * debian/control
    - bump Standards-Version to 3.9.5

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Thu, 26 Dec 2013 23:00:22 +0900

hts-voice-nitech-jp-atr503-m001 (1.05-1~exp1) experimental; urgency=low

  * New upstream release
  * debian/compat
    - up to 9
  * debian/control
    - update Standards-Version to 3.9.4
  * debian/copyright
    - specify correct url of the Format
    - update year of copyright
  * debian/install
    - update for the new voice file
  * debian/rules
    - remove descriptions for developers
  * debian/watch
    - remove descriptions for developers

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Wed, 16 Jan 2013 18:14:47 +0900

hts-voice-nitech-jp-atr503-m001 (1.04-1) unstable; urgency=low

  * Initial release (Closes: #643329)

 -- Koichi Akabe <vbkaisetsu@gmail.com>  Fri, 7 Oct 2011 23:25:51 +0900
